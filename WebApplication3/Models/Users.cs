﻿using System;
using System.Collections.Generic;

namespace WebApplication3.Models
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public DateTime Dob { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AdditionalText { get; set; }
    }
}
